#!/bin/bash 
# submit_prep_solvents.sh

## VARIABLES:
#   _JOB_NAME_ <-- job name
#   _NUM_CORES_ <-- number of cores
#   _INPUT_EQUIL_MDP_FILE_ <-- input equilibration file
#   _SOLVENT_NAME_ <-- solvent name
#   _NUM_OF_RES_ <-- number of residues
#   _TOPOLOGY_FILE_ <-- topology file
#   _PATH_SOLVENT_DIR_ <-- path t- solvent directory

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES
num_cores="_NUM_CORES_"

###SERVER_SPECIFIC_COMMANDS_END

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname

## DEFINING VARIABLES
input_equil_mdp_file="_INPUT_EQUIL_MDP_FILE_"
solvent_name="_SOLVENT_NAME_"
num_of_res="_NUM_OF_RES_"

topology_file="_TOPOLOGY_FILE_"
path_solvent_dir="_PATH_SOLVENT_DIR_"

## RUNNING EQUILIBRATION
gmx grompp -f "${input_equil_mdp_file}" -c "${solvent_name}_${num_of_res}_em.gro" -p "${topology_file}" -o "${solvent_name}_${num_of_res}_equil"
gmx mdrun -v -nt "${num_cores}" -deffnm "${solvent_name}_${num_of_res}_equil"

## LASTLY, COPYING EQUILIBRATED FILES TO MAIN DIRECTORY
# GRO FILE
cp -r "${solvent_name}_${num_of_res}_equil.gro" "${path_solvent_dir}/${solvent_name}.gro"
# ITP FILE
cp -r "${solvent_name}.itp" "${path_solvent_dir}/${solvent_name}.itp"
# PRM FILE
cp -r "${solvent_name}.prm" "${path_solvent_dir}/${solvent_name}.prm"