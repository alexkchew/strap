#!/bin/bash 
# submit_polymer_solvent.sh
# This script is to run polymer in solvent system

## VARIABLES:
#   _JOB_NAME_ <-- job name
#   _NUM_CORES_ <-- number of cores
#   _EQUILMDPFILE_ <-- input equilibration file
#   _PRODMDPFILE_ <-- input production file
#   _TOPFILE_ <-- topology file
#   _OUTPUTPREFIX_ <-- output prefix for the job
#   _EM_GRO_FILE_ <-- energy minimizaed gro file
#   _MAXWARN_ <-- number of maxwarns for thesystem

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES
num_cores="_NUM_CORES_"

###SERVER_SPECIFIC_COMMANDS_END

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname

## DEFINING PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING DEFAULT FILES
em_gro_file="_EM_GRO_FILE_"
top_file="_TOPFILE_"

## DEFINING MAX WARN
max_warn="_MAXWARN_"

## MDPFILES
equil_mdp_file="_EQUILMDPFILE_"
prod_mdp_file="_PRODMDPFILE_"

#####################
### EQUILIBRATION ###
#####################
gmx grompp -f ${equil_mdp_file} -c "${em_gro_file}" -p "${top_file}" -o "${output_prefix}_equil" -maxwarn "${max_warn}"

### RUNNING
if [ ! -e "${output_prefix}_equil.gro" ]; then
	## SEEING IF XTC FILE EXISTS
	if [ -e "${output_prefix}_equil.xtc" ]; then
		## RESTARTING
		gmx mdrun -nt "${num_cores}" -v -s "${output_prefix}_equil.tpr" -cpi "${output_prefix}_equil.cpt" -append -deffnm "${output_prefix}_equil"
	else
		## STARTING
		gmx mdrun -nt "${num_cores}" -v -deffnm "${output_prefix}_equil"
	fi
fi

##################
### PRODUCTION ###
##################
gmx grompp -f ${prod_mdp_file} -c "${output_prefix}_equil.gro" -p "${top_file}" -o "${output_prefix}_prod" -maxwarn "${max_warn}"

## SEEING IF GRO FILE EXISTS
if [ ! -e "${output_prefix}_prod.gro" ]; then
	## SEEING IF XTC FILE EXISTS
	if [ -e "${output_prefix}_prod.xtc" ]; then
		## RESTARTING
		gmx mdrun -nt "${num_cores}" -v -s "${output_prefix}_prod.tpr" -cpi "${output_prefix}_prod.cpt" -append -deffnm "${output_prefix}_prod"
	else
		## STARTING
		gmx mdrun -nt "${num_cores}" -v -deffnm "${output_prefix}_prod"
	fi
else
	echo "Job has completed!"
fi


