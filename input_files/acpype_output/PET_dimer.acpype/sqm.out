            --------------------------------------------------------
                             AMBER SQM VERSION 17
 
                                     By
              Ross C. Walker, Michael F. Crowley, Scott Brozell,
                         Tim Giese, Andreas W. Goetz,
                        Tai-Sung Lee and David A. Case
 
            --------------------------------------------------------
 

--------------------------------------------------------------------------------
  QM CALCULATION INFO
--------------------------------------------------------------------------------

| QMMM: Citation for AMBER QMMM Run:
| QMMM: R.C. Walker, M.F. Crowley and D.A. Case, J. COMP. CHEM. 29:1019, 2008

QMMM: SINGLET STATE CALCULATION
QMMM: RHF CALCULATION, NO. OF DOUBLY OCCUPIED LEVELS = 73

| QMMM: *** Selected Hamiltonian *** 
| QMMM: AM1         

| QMMM: *** Parameter sets in use ***
| QMMM: H : M.J.S.DEWAR et al. JACS, 107, 3902, (1985)
| QMMM: C : M.J.S.DEWAR et al. JACS, 107, 3902, (1985)
| QMMM: O : M.J.S.DEWAR et al. JACS, 107, 3902, (1985)

| QMMM: *** SCF convergence criteria ***
| QMMM: Energy change                :  0.1D-09 kcal/mol
| QMMM: Error matrix |FP-PF|         :  0.1D+00 au
| QMMM: Density matrix change        :  0.5D-06
| QMMM: Maximum number of SCF cycles :     1000

| QMMM: *** Diagonalization Routine Information ***
| QMMM: Pseudo diagonalizations are allowed.
| QMMM: Auto diagonalization routine selection is in use.
| QMMM:
| QMMM: Timing diagonalization routines:
| QMMM:                              norbs =      130
| QMMM:    diag iterations used for timing =       10
| QMMM:
| QMMM:              Internal diag routine = 0.047545 seconds
| QMMM:                 Dspev diag routine = 0.053619 seconds
| QMMM:                Dspevd diag routine = 0.033779 seconds
| QMMM:                Dspevx diag routine = 0.169590 seconds
| QMMM:                 Dsyev diag routine = 0.059882 seconds
| QMMM:                Dsyevd diag routine = 0.039979 seconds
| QMMM:                Dsyevr diag routine = 0.043702 seconds
| QMMM:
| QMMM:                Pseudo diag routine = 0.024027 seconds
| QMMM:
| QMMM: Using dspevd routine (diag_routine=3).

  QMMM: QM Region Cartesian Coordinates (*=link atom) 
  QMMM: QM_NO.   MM_NO.  ATOM         X         Y         Z
  QMMM:     1        1      H       -0.2120   -3.8310   -7.2040
  QMMM:     2        2      C       -1.1350   -4.3300   -6.8920
  QMMM:     3        3      H       -2.0000   -3.9110   -7.4150
  QMMM:     4        4      O       -1.3060   -4.2260   -5.4770
  QMMM:     5        5      C       -1.4160   -2.9510   -5.0290
  QMMM:     6        6      O       -1.3790   -1.9440   -5.7190
  QMMM:     7        7      C       -1.5900   -2.9260   -3.5510
  QMMM:     8        8      C       -1.9260   -2.7660   -0.7600
  QMMM:     9        9      C       -1.7180   -1.6800   -2.9200
  QMMM:    10       10      C       -1.6310   -4.0930   -2.7770
  QMMM:    11       11      C       -1.7980   -4.0120   -1.3890
  QMMM:    12       12      C       -1.8850   -1.5990   -1.5330
  QMMM:    13       13      H       -1.6880   -0.7630   -3.5080
  QMMM:    14       14      H       -1.5350   -5.0740   -3.2370
  QMMM:    15       15      H       -1.8280   -4.9290   -0.8020
  QMMM:    16       16      H       -1.9810   -0.6180   -1.0740
  QMMM:    17       17      C       -2.0990   -2.7410    0.7190
  QMMM:    18       18      O       -2.1210   -3.7440    1.4130
  QMMM:    19       19      O       -2.2160   -1.4670    1.1640
  QMMM:    20       20      C       -2.3830   -1.3190    2.5880
  QMMM:    21       21      H       -2.8220   -0.3280    2.7400
  QMMM:    22       22      H       -3.1020   -2.0550    2.9660
  QMMM:    23       23      C       -1.0420   -1.4350    3.3120
  QMMM:    24       24      H       -0.5620   -2.3930    3.0880
  QMMM:    25       25      H       -1.1740   -1.3830    4.3990
  QMMM:    26       26      O       -0.1250   -0.4160    2.8690
  QMMM:    27       27      C       -0.3030    0.8100    3.4190
  QMMM:    28       28      O       -1.1590    1.1070    4.2370
  QMMM:    29       29      C        0.6940    1.7760    2.8820
  QMMM:    30       30      C        2.5320    3.6830    1.9280
  QMMM:    31       31      C        1.6550    1.4140    1.9290
  QMMM:    32       32      C        0.6570    3.0950    3.3540
  QMMM:    33       33      C        1.5710    4.0440    2.8810
  QMMM:    34       34      C        2.5680    2.3630    1.4560
  QMMM:    35       35      H        1.7080    0.3990    1.5430
  QMMM:    36       36      H       -0.0840    3.3920    4.0950
  QMMM:    37       37      H        1.5180    5.0600    3.2670
  QMMM:    38       38      H        3.3100    2.0670    0.7150
  QMMM:    39       39      C        3.5290    4.6500    1.3900
  QMMM:    40       40      O        4.3740    4.3520    0.5610
  QMMM:    41       41      O        3.3570    5.8750    1.9450
  QMMM:    42       42      C        4.2800    6.8590    1.4760
  QMMM:    43       43      H        4.1650    7.0090    0.3970
  QMMM:    44       44      H        5.3070    6.5690    1.7200
  QMMM:    45       45      H       -1.0580   -5.3910   -7.1460
  QMMM:    46       46      H        4.0560    7.8020    1.9820

--------------------------------------------------------------------------------
  RESULTS
--------------------------------------------------------------------------------

      iter         sqm energy              rms gradient
      ----    -------------------    -----------------------
xmin    10     -279.7328 kcal/mol        0.0879 kcal/(mol*A)
xmin    20     -279.7386 kcal/mol        0.1530 kcal/(mol*A)
xmin    30     -279.7515 kcal/mol        0.0957 kcal/(mol*A)
xmin    40     -279.7571 kcal/mol        0.0487 kcal/(mol*A)
xmin    50     -279.7623 kcal/mol        0.0264 kcal/(mol*A)
xmin    60     -279.7647 kcal/mol        0.0237 kcal/(mol*A)
xmin    70     -279.7711 kcal/mol        0.0715 kcal/(mol*A)
xmin    80     -279.7726 kcal/mol        0.0209 kcal/(mol*A)
xmin    90     -279.7747 kcal/mol        0.0185 kcal/(mol*A)
xmin   100     -279.7750 kcal/mol        0.0071 kcal/(mol*A)
xmin   110     -279.7751 kcal/mol        0.0120 kcal/(mol*A)
xmin   120     -279.7757 kcal/mol        0.0095 kcal/(mol*A)
xmin   130     -279.7758 kcal/mol        0.0056 kcal/(mol*A)
xmin   140     -279.7763 kcal/mol        0.0094 kcal/(mol*A)
xmin   150     -279.7765 kcal/mol        0.0206 kcal/(mol*A)
xmin   160     -279.7769 kcal/mol        0.0111 kcal/(mol*A)
xmin   170     -279.7769 kcal/mol        0.0021 kcal/(mol*A)
xmin   180     -279.7769 kcal/mol        0.0017 kcal/(mol*A)
xmin   190     -279.7770 kcal/mol        0.0020 kcal/(mol*A)
xmin   200     -279.7770 kcal/mol        0.0029 kcal/(mol*A)
xmin   210     -279.7770 kcal/mol        0.0012 kcal/(mol*A)
xmin   220     -279.7770 kcal/mol        0.0049 kcal/(mol*A)
xmin   230     -279.7770 kcal/mol        0.0028 kcal/(mol*A)
xmin   240     -279.7770 kcal/mol        0.0014 kcal/(mol*A)
xmin   250     -279.7770 kcal/mol        0.0008 kcal/(mol*A)
  ... geometry converged !
 
 Final MO eigenvalues (eV):
 -41.7478  -41.4583  -41.1845  -40.8628  -39.6166  -39.3480  -37.5144  -37.1464
 -37.1181  -36.9725  -33.5890  -33.3129  -32.1756  -32.1557  -31.1811  -28.8335
 -28.6185  -26.8641  -26.2186  -24.8179  -23.9947  -23.7956  -23.1390  -22.3565
 -21.6467  -21.2313  -19.3262  -19.3014  -19.1099  -18.8654  -18.0940  -18.0362
 -17.8818  -17.6610  -17.6389  -17.2391  -17.2343  -16.5084  -16.1467  -16.1401
 -15.9149  -15.8687  -15.7045  -15.2585  -14.9315  -14.9125  -14.8364  -14.6447
 -14.4961  -14.4166  -14.2651  -13.7311  -13.6843  -13.6236  -13.4762  -13.2196
 -13.2098  -13.1516  -12.9739  -12.5576  -12.5150  -12.3885  -11.8083  -11.7044
 -11.7044  -11.6910  -11.5766  -11.5435  -11.2686  -10.4981  -10.4865  -10.3499
 -10.3498   -1.1191   -1.1078   -0.1023   -0.1023    0.7659    0.7949    1.5321
   1.5590    1.5845    1.6942    1.7551    1.7591    2.4245    2.4316    3.0889
   3.1021    3.2458    3.3023    3.3944    3.4042    3.4293    3.4858    3.5623
   3.6397    3.7806    3.7813    3.8764    3.9045    3.9221    3.9280    4.0089
   4.0604    4.0836    4.0836    4.1831    4.1853    4.2423    4.2593    4.3361
   4.4966    4.5035    4.6245    4.6375    4.8107    4.8356    4.8543    4.8747
   5.1146    5.2528    5.2531    5.2760    5.2824    5.3148    6.4652    6.5198
   6.5529    6.5529

 Heat of formation   =       -279.77704670 kcal/mol  (      -12.13204313 eV)

 Total SCF energy    =    -123881.74785598 kcal/mol  (    -5371.91569559 eV)
 Electronic energy   =    -827695.97849583 kcal/mol  (   -35891.59093256 eV)
 Core-core repulsion =     703814.23063985 kcal/mol  (    30519.67523697 eV)
 
    Atomic Charges for Step       1 :
  Atom    Element       Mulliken Charge
     1      H                  0.088
     2      C                 -0.062
     3      H                  0.088
     4      O                 -0.275
     5      C                  0.340
     6      O                 -0.345
     7      C                 -0.080
     8      C                 -0.084
     9      C                 -0.093
    10      C                 -0.088
    11      C                 -0.092
    12      C                 -0.086
    13      H                  0.165
    14      H                  0.163
    15      H                  0.164
    16      H                  0.164
    17      C                  0.345
    18      O                 -0.355
    19      O                 -0.267
    20      C                 -0.039
    21      H                  0.134
    22      H                  0.113
    23      C                 -0.039
    24      H                  0.134
    25      H                  0.113
    26      O                 -0.267
    27      C                  0.345
    28      O                 -0.355
    29      C                 -0.084
    30      C                 -0.080
    31      C                 -0.086
    32      C                 -0.092
    33      C                 -0.088
    34      C                 -0.093
    35      H                  0.164
    36      H                  0.164
    37      H                  0.163
    38      H                  0.165
    39      C                  0.340
    40      O                 -0.345
    41      O                 -0.275
    42      C                 -0.062
    43      H                  0.088
    44      H                  0.088
    45      H                  0.104
    46      H                  0.104
 Total Mulliken Charge =       0.000
 
                  X        Y        Z     TOTAL  
  QM DIPOLE     0.029    0.023   -0.050    0.062
 
 Final Structure

  QMMM: QM Region Cartesian Coordinates (*=link atom) 
  QMMM: QM_NO.   MM_NO.  ATOM         X         Y         Z
  QMMM:     1        1      H       -0.4649   -4.0835   -7.3597
  QMMM:     2        2      C       -1.3741   -4.6156   -6.9898
  QMMM:     3        3      H       -2.2865   -4.2062   -7.4862
  QMMM:     4        4      O       -1.4826   -4.4668   -5.5725
  QMMM:     5        5      C       -1.6015   -3.1855   -5.1017
  QMMM:     6        6      O       -1.6062   -2.2663   -5.9252
  QMMM:     7        7      C       -1.7098   -3.0859   -3.6370
  QMMM:     8        8      C       -1.9197   -2.8230   -0.8701
  QMMM:     9        9      C       -1.8320   -1.8132   -3.0635
  QMMM:    10       10      C       -1.6924   -4.2268   -2.8265
  QMMM:    11       11      C       -1.7971   -4.0958   -1.4435
  QMMM:    12       12      C       -1.9370   -1.6820   -1.6804
  QMMM:    13       13      H       -1.8442   -0.9234   -3.7146
  QMMM:    14       14      H       -1.5958   -5.2240   -3.2859
  QMMM:    15       15      H       -1.7845   -4.9856   -0.7925
  QMMM:    16       16      H       -2.0332   -0.6847   -1.2210
  QMMM:    17       17      C       -2.0277   -2.7236    0.5948
  QMMM:    18       18      O       -2.0028   -3.6444    1.4173
  QMMM:    19       19      O       -2.1765   -1.4438    1.0600
  QMMM:    20       20      C       -2.3111   -1.2647    2.4739
  QMMM:    21       21      H       -2.7244   -0.2228    2.5587
  QMMM:    22       22      H       -3.0284   -2.0193    2.8906
  QMMM:    23       23      C       -0.9770   -1.3830    3.1927
  QMMM:    24       24      H       -0.4426   -2.3274    2.8992
  QMMM:    25       25      H       -1.1443   -1.3408    4.3007
  QMMM:    26       26      O       -0.0606   -0.3575    2.7952
  QMMM:    27       27      C       -0.2441    0.8943    3.3202
  QMMM:    28       28      O       -1.1930    1.0710    4.0904
  QMMM:    29       29      C        0.7533    1.8838    2.8802
  QMMM:    30       30      C        2.6077    3.8089    2.0898
  QMMM:    31       31      C        1.7656    1.5442    1.9754
  QMMM:    32       32      C        0.6681    3.1866    3.3897
  QMMM:    33       33      C        1.5952    4.1484    2.9946
  QMMM:    34       34      C        2.6926    2.5063    1.5801
  QMMM:    35       35      H        1.8250    0.5174    1.5791
  QMMM:    36       36      H       -0.1351    3.4396    4.1015
  QMMM:    37       37      H        1.5357    5.1752    3.3904
  QMMM:    38       38      H        3.4956    2.2535    0.8680
  QMMM:    39       39      C        3.6047    4.7985    1.6498
  QMMM:    40       40      O        4.5424    4.6307    0.8652
  QMMM:    41       41      O        3.4420    6.0443    2.1966
  QMMM:    42       42      C        4.3864    7.0409    1.7996
  QMMM:    43       43      H        4.3325    7.1976    0.6955
  QMMM:    44       44      H        5.4169    6.7265    2.0924
  QMMM:    45       45      H       -1.2899   -5.7213   -7.1306
  QMMM:    46       46      H        4.0582    7.9500    2.3610

           --------- Calculation Completed ----------

