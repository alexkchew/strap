; equil.mdp
; This script is an MDP file for the AMBER force field. 
; This equilibrates with a pressure coupling of berendsen and temperature coupling of velocity rescale. 
; Variables to edit:
;	_NSTEPS_ <-- number of steps in the simulation
; 	_TEMPERATURE_ <-- temperature of the system

; Run control
integrator  = md
tinit       = 0
nsteps      = _NSTEPS_ ; 20 ns
dt          = 0.002

; Removal of center of mass motion
comm_mode   = Linear ; Remove center of mass translational velocity
nstcomm     = 100 ; default steps for center of mass motion removal

; Ouput control
nstxout     = 0
nstvout     = 0
nstfout     = 0
nstxtcout   = 5000 ; 10 ps output
nstenergy   = 500

; General options
continuation = no
pbc          = xyz
gen-vel      = yes	; assign velocities from Maxwell distribution

; Neighbor list options
cutoff-scheme = Verlet
nstlist       = 10		; recommended for verlet algorithm
ns-type       = grid

; Electrostatics
coulombtype      = PME
coulomb-modifier = Potential-shift-Verlet
rcoulomb         = 1.2
fourierspacing   = 0.14
ewald-rtol       = 1e-6
pme-order        = 4

; vdW interactions
vdwtype      = Cut-off
vdw-modifier = Potential-shift-Verlet
rvdw         = 1.2
DispCorr     = No

; Thermostat
tcoupl          = v-rescale
tc-grps         = System
ref-t           = _TEMPERATURE_
tau-t           = 2.0
nh-chain-length = 1

; Barostat
pcoupl          = berendsen
pcoupltype      = isotropic
ref_p           = 1.0
tau-p           = 2.0
compressibility = 3e-5
