#!/bin/bash

# prep_polymer_solvent.sh
# This script runs polymer within solvent systems.
# Algorithm:
#   - Generate polymer system
#   - Define distance to the edge
#   - Generate polymer in empty box with distance to edge
#   - Solvate with packmol or gmx solvate
#   - Generate appropiate topology file
#   - Run polymer in topology

## VARIABLES
#   $1: polymer_name
#   $2: solvent name

# Written by: Alex K. Chew (10/06/2019)

## DEFINING OUTPUT NAME
function get_output_polymer_in_solvent () {
    ## DEFINING INPUTS
    polymer_name_="$1"
    solvent_name_="$2"
    temp_="$3"
    
    ## DEFINING OUTPUT NAME
    output_name_="P_in_S-${polymer_name_}-${solvent_name_}-${temp_}_K"
    
    ## PRINTING
    echo "${output_name_}"
}

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bashrc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING MOLECULE NAME
polymer_name="$1"
# "10_mer_PE"

## DEFINING RESIDUE NAME
solvent_name="$2"
# "toluene"

## DEFINING DISTANCE TO EDGE
dist_to_edge="1"

## DEFINING TEMPERATURE
temp="300"

#########################
### GETTING MDP / TOP ###
#########################

## DEFINING TOP FILE
input_top_file="polymer_solvent_equil.top"

## DEFINING INPUT TOP PATH
input_top_path="${PATH_TOP_FILES}/${input_top_file}"

## DEFINING MDP FILE PATH
mdp_file_path="${PATH_MDP_FILES}"

## DEFINING MDP FILES
mdp_em="em.mdp"
mdp_equil="equil.mdp"
mdp_prod="prod.mdp"

## DEFINING NUMBER OF MDP FILE STEPS
mdp_equil_nsteps="1000000" # 2 ns # "2500000" # 5 ns
# 5000000 <-- 10 ns
mdp_temp="${temp}" # Temperature at 300 K
mdp_prod_nsteps="5000000" # 10 ns

## DEFINING SUBMIT FILE
input_submit_file="submit_polymer_solvent.sh"
input_submit_path="${PATH_SUBMIT_FILES}/${input_submit_file}"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING NUMBER OF CORES
num_cores="28"

## DEFINING MAX WARNS
max_warn="10"

## DEFINING NUMBER OF DESIRED SOLVENTS
num_of_res="216"

## DEFINING TEMP
default_temp="300"

## DEFINING OUTPUT DIRECTORY
solvent_dir_name="${solvent_name}-${num_of_res}_res-${default_temp}_K"

## DEFINING PYTHON FILES
py_count_gro_residues="${PY_COUNT_GRO_FILE}"

######################
### DEFINING PATHS ###
######################

## DEFINING SOLVENT PATH
path_solvent="${PATH_PREP_FILES_SOLVENT}/${solvent_dir_name}"
stop_if_does_not_exist "${path_solvent}"

## PATH TO POLYMER
path_polymer="${PATH_PREP_FILES_MOLECULES}/${polymer_name}"
stop_if_does_not_exist "${path_polymer}"

#####################
### DEFAULT FILES ###
#####################

## ITP, GRO, PARM
polymer_gro="${polymer_name}.gro"
polymer_itp="${polymer_name}.itp"
polymer_prm="${polymer_name}.prm"

########################
### OUTPUT VARIABLES ###
########################

## GETTING OUTPUT DIR NAME
output_dir_name=$(get_output_polymer_in_solvent "${polymer_name}" "${solvent_name}" "${temp}")

## DEFINING OUTPUT PATH
output_path="${PATH_POLYMER_SOLVENT}/${output_dir_name}"

## DEFINING OUTPUT PREFIX
output_prefix="poly_solv"

## OUTPUT  FILE
output_top="${output_prefix}.top"
output_submit="submit.sh"

###################
### MAIN SCRIPT ###
###################

## CREATING DIRECTORY
create_dir "${output_path}" -f

## GOING TO DIRECTORY
cd "${output_path}"

## COPYING OVER GRO FILE
cp -r "${path_polymer}/${polymer_gro}" "${output_prefix}.gro"

## GMX EDITCONF TO GET EXPANDED VERSION
gmx editconf -f "${output_prefix}.gro" -o "${output_prefix}_expand.gro" -d "${dist_to_edge}" -bt cubic -c

## GETTING GRO BOX SIZE
read -a box_size <<< $(gro_measure_box_size "${output_prefix}_expand.gro")

## SOLVATING
gmx solvate -cp "${output_prefix}_expand.gro" -cs "${path_solvent}/${solvent_name}.gro" -o "${output_prefix}_solvated.gro" 

## DEFINING GRO SUMMARY FILE
gro_summary="${output_prefix}_solvated.summary"

########################
### ADDING MDP FILES ###
########################
echo "** CORRECTING MDP FILE FILE **"
# MDP FILES
cp -r "${mdp_file_path}/"{${mdp_em},${mdp_equil},${mdp_prod}} "${output_path}"

## EDITING MDP FILES
sed -i "s/_NSTEPS_/${mdp_equil_nsteps}/g" "${mdp_equil}"
sed -i "s/_TEMPERATURE_/${mdp_temp}/g" "${mdp_equil}"

## EDITING MDP FILES
sed -i "s/_NSTEPS_/${mdp_prod_nsteps}/g" "${mdp_prod}"
sed -i "s/_TEMPERATURE_/${mdp_temp}/g" "${mdp_prod}"

############################
### ADDING TOPOLOGY FILE ###
############################
echo "** CORRECTING TOPOLOGY FILE **"
## COPYING TOPOLOGY FILE
cp -r "${input_top_path}" "${output_path}/${output_top}"

## COPYING ITP FILE AND PARAMETER FILES
cp -r "${path_polymer}/"{${polymer_itp},${polymer_prm}} "${output_path}"
cp -r "${path_solvent}"/{${solvent_name}.itp,${solvent_name}.prm} "${output_path}"

## FINDING RESIDUE NAMES OF EACH
polymer_residue_name=$(itp_get_resname ${polymer_itp})
solvent_residue_name=$(itp_get_resname ${solvent_name}.itp)

## DEFINING RESIDUE LIST
declare -a residue_list=("${polymer_residue_name}" "${solvent_residue_name}")

## EDITING TOP FILE
sed -i "s/_POLYMERNAME_/${polymer_name}/g" "${output_top}"
sed -i "s/_SOLVENTNAME_/${solvent_name}/g" "${output_top}"

## RUNNING PYTHON SCRIPT TO COUNT GRO FILE
/usr/bin/python3.4 "${py_count_gro_residues}" \
        --igro "${output_prefix}_solvated.gro"\
        --sum "${gro_summary}"
        
## ADDING SUMMARY RESIDUES
for each_residue in "${residue_list[@]}"; do
    ## LOCATING RESIDUE NUMBER
    tot_residue=$(grep ${each_residue} "${gro_summary}" | awk '{print $2}')
    ## ADDING TO TOP FILE
    echo "   ${each_residue} ${tot_residue}" >> "${output_top}"
done

##############################
### ADDING SUBMISSION FILE ###
##############################
echo "** CORRECTING SUBMIT FILE **"
## COPYING SUBMIT FILE
cp -r "${input_submit_path}" "${output_path}/${output_submit}"

## EDITING FILE
## EDITING TOP FILE
sed -i "s/_NUM_CORES_/${num_cores}/g" "${output_submit}"
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${output_submit}"
sed -i "s/_EM_GRO_FILE_/${output_prefix}_em.gro/g" "${output_submit}"
sed -i "s/_MAXWARN_/${max_warn}/g" "${output_submit}"
sed -i "s/_TOPFILE_/${output_top}/g" "${output_submit}"
sed -i "s/_EQUILMDPFILE_/${mdp_equil}/g" "${output_submit}"
sed -i "s/_PRODMDPFILE_/${mdp_prod}/g" "${output_submit}"
sed -i "s/_JOB_NAME_/${output_dir_name}/g" "${output_submit}"

###################################
### RUNNING ENERGY MINIMIZATION ###
###################################
## GROMPP
gmx grompp -f ${mdp_em} -c "${output_prefix}_solvated.gro" -p "${output_top}" -o "${output_prefix}_em" -maxwarn "${max_warn}"
## MDRUN
gmx mdrun -v -nt 1 -deffnm "${output_prefix}_em"

##########################
### ADDING TO JOB LIST ###
##########################

echo -e "--- CREATING SUBMISSION FILE: ${output_submit} ---"
echo "${output_path}/${output_submit}" >> ${JOB_SCRIPT}










