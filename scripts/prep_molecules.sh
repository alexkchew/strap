#!/bin/bash

# prep_molecules.sh
# The purpose of this code is to prepare AMBER force field molecules.
# Algorithm:
#   - Input molecule name and residue name
#   - Copy over itp file, gro file, top file, MDP files
#   - Use sed to change all residue names corresponding to the correct molecules
#   - Split into parameters and itp file
#   - Run energy minimization
#   - Output a *.gro, *.pdb, *.itp, and *.prm file, which could be used for subsequent sims.

# Written by: Alex K. Chew (10/06/2019)

## VARIABLES
#   $1: molecule name
#   $2: residue name

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bashrc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING MOLECULE NAME
molecule_name="$1"
# "toluene"

## DEFINING RESIDUE NAME
residue_name="$2"
# "TOL"

## CHECKING IF SOLVENT NAME INCLUDED
if [[ -z "${molecule_name}" ]] || [[ -z "${residue_name}" ]]; then
    echo "Error, input is incorrect!"
    echo "Usage: bash prep_molecules.sh MOLECULE_NAME RESIDUE_NAME"
    exit 1
fi

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING ACPYPE FOLDER
acpype_folder="${PATH_ACPYPE_OUTPUT}/${molecule_name}.acpype"

## SEEING IF EXIST
stop_if_does_not_exist "${acpype_folder}"

## DEFINING ACPYPE OUTPUT
input_molecule_itp="${molecule_name}_GMX.itp"
input_molecule_gro="${molecule_name}_GMX.gro"
input_molecule_top="${molecule_name}_GMX.top"

## DEFINING MDP FILES
em_mdp="em.mdp"
md_mdp="md.mdp"

########################
### OUTPUT VARIABLES ###
########################

## DEFINING OUTPUT VARIABLE
output_path="${PATH_PREP_FILES_MOLECULES}/${molecule_name}"

## DEFINING SETUP FILES NAME
setup_files="setup_files"

## DEFINING SETUP PATH
setup_path="${output_path}/${setup_files}"

## DEFINING OUTPUT PREFIX
output_prefix="${molecule_name}"

## DEFINING EM AND MD PREFIX
em_prefix="em"
md_prefix="md"

###################
### MAIN SCRIPT ###
###################

## CREATING DIRECTORY
create_dir "${output_path}" -f

## GOING INTO DIRECTORY AND MAKING SETUP DIR
mkdir -p "${setup_path}"
cd "${setup_path}"

## COPYING OVER FILES
echo "--> COPYING FILES FROM: ${acpype_folder}"
cp -r "${acpype_folder}/"{${input_molecule_itp},${input_molecule_gro},${input_molecule_top},${em_mdp},${md_mdp}} ${setup_path}

#########################################
### CHECKING FILE FOR CORRECT RESNAME ###
#########################################

## GETTING THE RESIDUE NAME OF THE ITP FILE
itp_file_res_name=$(itp_get_resname "${input_molecule_itp}") 

## EDITING THE ITP FILE
if [[ "${itp_file_res_name}" != "${residue_name}" ]]; then
    echo "----- CORRECTING RESIDUE NAME IN: ${input_molecule_itp}"
    sed -i "s/${itp_file_res_name}/${residue_name}/g" "${input_molecule_itp}"
    ## CHECKING FOR <0> ISSUES
    if [[ "$(grep "<0>" "${input_molecule_itp}" | wc -l)" != 0 ]]; then
        ## FIXING FOR ISSUES WITH 0
        sed -i "s#<0>#${residue_name}#g" "${input_molecule_itp}"
    fi
    
    ## FINDING ITP FILE RESNAME
    itp_resname_in_atom=$(itp_get_residue_name_within_atom "${input_molecule_itp}")
    
    ## CHECKING FOR LIG ISSUES
    if [[ "${itp_resname_in_atom}" != "${residue_name}" ]]; then
        ## FIXING FOR ISSUES WITH 0
        sed -i "s#${itp_resname_in_atom}#${residue_name}#g" "${input_molecule_itp}"
    fi
    
fi

## GETTING RESIDUE NAME OF GRO FILE
gro_file_res_name=$(gro_get_first_residue_name ${input_molecule_gro})
# echo "${gro_file_res_name}"; echo "${residue_name}" ; sleep 5

## EDITING THE GRO FILE
if [[ "${gro_file_res_name}" != "${residue_name}" ]]; then
    echo "----- CORRECTING RESIDUE NAME IN: ${input_molecule_gro}"
    sed -i "s/${gro_file_res_name}/${residue_name}/g" "${input_molecule_gro}"
fi

## EXTRACT PARAMETER FILE FROM ITP FILE
itp_extract_prm "${input_molecule_itp}"

## DEFINING PARMAETER FILE
input_molecule_prm="${input_molecule_itp%.itp}.prm"

## CORRECTING TOPOLOGY FILE
echo "----- CORRECTING TOPOLOGY IN : ${input_molecule_top}"
# RESIDUE NAME
sed -i "s/\b${molecule_name}\b/${residue_name}/g" "${input_molecule_top}"

# ADDING PARAMETER FILE
topology_add_b4_include_specific "${input_molecule_top}" "#include \"${input_molecule_itp}\"" "#include \"${input_molecule_prm}\""

###################################
### RUNNING ENERGY MINIMIZATION ###
###################################

## ENERGY MINIMIZATION GROMPP
gmx grompp -f "${em_mdp}" -c "${input_molecule_gro}" -p "${input_molecule_top}" -o ${em_prefix}.tpr -v

## RUNNING ENERGY MINIMIZATION
gmx mdrun -nt 1 -v -deffnm "${em_prefix}"

## CHECKING IF COMPLETING
stop_if_does_not_exist "${em_prefix}.gro"

########################
### RUNNING QUICK MD ###
########################
## GROMPP MD
gmx grompp -f "${md_mdp}" -c "${em_prefix}.gro" -p "${input_molecule_top}" -o "${md_prefix}.tpr"
gmx mdrun -nt 4 -v -deffnm "${md_prefix}"

## CHECKING IF COMPLETING
stop_if_does_not_exist "${md_prefix}.gro"

###################
### WRAPPING UP ###
###################

## COPYING OVER GRO FILE
cp "${md_prefix}.gro" "${output_path}/${output_prefix}.gro"
cp "${input_molecule_prm}" "${output_path}/${output_prefix}.prm"
cp "${input_molecule_itp}" "${output_path}/${output_prefix}.itp"
## MAKING PDB FILE
cd "${output_path}"
gmx editconf -f "${output_prefix}.gro" -o "${output_prefix}.pdb"

########################
### PRINTING SUMMARY ###
########################
echo "*** SUMMARY ***"
echo "Created molecule: ${molecule_name}"
echo "Residue name: ${residue_name}"
echo "Location: ${output_path}"
echo "  GRO: ${output_prefix}.gro"
echo "  PRM: ${output_prefix}.prm"
echo "  ITP: ${output_prefix}.itp"
echo "  PDB: ${output_prefix}.pdb"



