#!/bin/bash

# bashrc.sh
# This contains all functions / global variables
# Created by: Alex K. Chew (10/06/2019)

## PRINTING
echo "-------- Reloading bashrc.sh --------"

#################################
### GETTING CURRENT DIRECTORY ###
#################################

## DEFINING MAIN DIRECTORY
dir_loc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING MAIN DIRECTORY
main_dir="${dir_loc}/../../"

### LOADING GLOBAL FUNCTIONS ACROSS ALL PROJECTS
source "${HOME}/bin/bashfiles/server_general_research_functions.sh"

## DEFINING CURRENT WORKING DIRECTORY
export CURRENTWD=$(cd ${main_dir}; pwd)

## DEFINING SCRIPTS DIRECTORY
export PATH2SCRIPTS="${CURRENTWD}/scripts"

## DEFINING BIN DIRECTORY
export PATH2BIN="${PATH2SCRIPTS}/bin"

## DEFINING MAJOR FILES TO LOAD
### LOADING GLOBAL VARIABLES
source "${PATH2BIN}/global_vars.sh"
### LOADING GLOBAL GENERAL FUNCTIONS
source "${PATH2BIN}/functions.sh"
