#!/bin/bash

# prep_solvent_216.sh
# This code runs preparation of solvents for 216 residues. 
# Algorithm:
#   - Define solvent of interest
#   - Get the approximate molecular volume using editconf
#   - Determine box size required
#   - Add the solvent into system using PACKMOL
#   - Solvate and equilibrate for 5 ns

# Written by: Alex K. Chew (10/06/2019)

## VARIABLES
#   $1: molecule name

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="bashrc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING SOLVENT NAME
solvent_name="$1"

## CHECKING IF SOLVENT NAME INCLUDED
if [[ -z "${solvent_name}" ]]; then
    echo "Error, solvent name not included! Stopping here!"
    echo "Usage: bash prep_solvent_216.sh SOLVENT_NAME"
    exit 1
fi

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING TEMPERATURE TO SOLVATE
temp="300" # Kelvins

## DEFINING NUMBER OF DESIRED SOLVENTS
num_of_res="216"

## SEEING IF YOU WANT SUBMISSION
want_submit=true # true

## DEFINING FUDGE FACTOR
fudge_fact="1.2" # Used to fudge volume slightly higher -- no fudging
# 1.5

## DEFINING NUMBER OF CORES
num_cores="28"

##################################
### DEFINING INPUT DIRECTORIES ###
##################################

## PATH TO INPUT MOLECULE
path_input_molecule="${PATH_PREP_FILES_MOLECULES}/${solvent_name}"

## SEEING IF EXIST
stop_if_does_not_exist "${path_input_molecule}"

## DEFINING ACPYPE OUTPUT
input_molecule_prm="${solvent_name}.prm"
input_molecule_itp="${solvent_name}.itp"
input_molecule_gro="${solvent_name}.gro"
input_molecule_top="${solvent_name}.top"

####################
### MDP/TOP FILE ###
####################

## DEFINING MDP FILE PATH
mdp_file_path="${PATH_MDP_FILES}"

## DEFINING MDP FILES
mdp_em="em.mdp"
mdp_equil="equil.mdp"

## DEFINING NUMBER OF MDP FILE STEPS
mdp_equil_nsteps="5000000" # 2 ns # "2500000" # 5 ns
# 5000000 <-- 10 ns
mdp_temp="${temp}" # Temperature at 300 K

## DEFINING TOP FILE PATH
top_file_path="${PATH_TOP_FILES}"

## DEFINING TOP FILE
input_top_file="em_equil.top"

########################
### OUTPUT VARIABLES ###
########################

## DEFINING OUTPUT DIRECTORY
output_dir_name="${solvent_name}-${num_of_res}_res-${temp}_K"

## DEFINING OUTPUT VARIABLE
output_path="${PATH_PREP_FILES_SOLVENT}/${output_dir_name}"

## DEFINING SETUP FILES NAME
setup_files="em_equil"

## DEFINING SETUP PATH
setup_path="${output_path}/${setup_files}"

## DEFINING OUTPUT PREFIX
output_prefix="solv"

## DEFINING TOPOLOGY FILE
topology_file="${output_prefix}.top"

## DEFINING EM AND MD PREFIX
em_prefix="em"
md_prefix="md"

#######################
### SUBMISSION FILE ###
#######################
## DEFINING SUBMIT FILE PATH
submit_file_path="${PATH_SUBMIT_FILES}"
## DEFINING SUBMIT FILE
input_submit_file="submit_prep_solvents.sh"
output_submit_file="submit.sh"

###################
### MAIN SCRIPT ###
###################

## CREATING DIRECTORY
create_dir "${output_path}" -f

## GOING INTO DIRECTORY AND MAKING SETUP DIR
mkdir -p "${setup_path}"
cd "${setup_path}"

######################################################
#### -- SHRINKING AND FINDING MOLECULAR VOLUME -- ####
######################################################
## RUNNING EDITCONF TO GET MOLECULAR VOLUME
echo "Shrinking GRO file to get molecular volume, stored in ${solvent_name}_shrink.gro"
gmx editconf -f "${path_input_molecule}/${input_molecule_gro}" -o "${setup_path}/${solvent_name}_shrink.gro" -d 0.000 -bt cubic >/dev/null 2>&1
 
## READING BOX LENGTH AND BOX VOLUME
read -a box_length<<<$(read_gro_box_lengths ${solvent_name}_shrink.gro)
molecular_volume="$(compute_volume_from_box_length "${box_length[@]}")"

## FINDING TOTAL VOLUME WITH FUDGE FACTOR
new_volume=$(echo "${num_of_res}*${molecular_volume}" | bc -l)
new_length=$(awk -v vol=${new_volume} -v fudge_fact=${fudge_fact} 'BEGIN{ printf "%.4f",vol**(1/3)*fudge_fact}') # Fudging by 50%
volume_with_fudge=$(echo "${new_length}*${new_length}*${new_length}" | bc -l)

## PRINTING
echo "--- Shrinking to find molecular volume ---"
echo "Box length: ${box_length[@]}"
echo "Molecular volume: ${molecular_volume} nm3"
echo "With ${num_of_res} molecules: ${new_volume} nm3"
echo "Using fudge factor of ${fudge_fact}, new box length is: ${new_length} nm, ${volume_with_fudge} nm3"
echo ""

## STORING INTO FILE
info_file="shrinking.info"
echo "--- Shrinking to find molecular volume ---" > "${info_file}"
echo "Box length: ${box_length[@]}" >> "${info_file}"
echo "Molecular volume: ${molecular_volume} nm3" >> "${info_file}"
echo "With ${num_of_res} molecules: ${new_volume} nm3" >> "${info_file}"
echo "Using fudge factor of ${fudge_fact}, new box length is: ${new_length} nm, ${volume_with_fudge} nm3" >> "${info_file}"
echo ""

###############
### PACKMOL ###
###############

## DEFINING PACKMOL SCRIPT
packmol_script="${PACKMOL_SCRIPT}"

## DEFINING PACKMOL INPUT
packmol_input="packmol_cosolvents.inp"
path_packmol_input="${INPUT_PACKMOL_PATH}/${packmol_input}"

## DEFINING PDB FILE
cosolvent_pdb="${solvent_name}.pdb"

## DEFINING IMPORTANT PATHS FOR PDB FILE
path_input_cosolvent="${path_input_molecule}/${cosolvent_pdb}"

## COPYING INPUT FILE
cp "${path_packmol_input}" "${setup_path}"

## COPYING PDB FILE
cp "${path_input_cosolvent}" "${setup_path}"

## DEFINING OUTPUT PDB
output_solvated_pdb="${solvent_name}_solvated.pdb"

## GETTING MAX LENGTH IN ANGS
new_length_angs=$(awk -v lengths=${new_length} 'BEGIN{ printf "%.5f",lengths*10}')

## EDITING PACKMOL FILE
# sed -i "s/_NPPDB_/${output_pdb}/g" 
sed -i "s/_OUTPUTPDB_/${output_solvated_pdb}/g" "${packmol_input}"
sed -i "s/_COSOLVENTPDB_/${cosolvent_pdb}/g" "${packmol_input}"
sed -i "s/_NUMCOSOLVENT_/${num_of_res}/g" "${packmol_input}"
# ANGS MAX
sed -i "s/_MAXANGS_/${new_length_angs}/g" "${packmol_input}"

## RUNNING PACKMOL
"${PACKMOL_SCRIPT}" < ${packmol_input}

# DEFINING NAME
gro_with_res_num="${solvent_name}_${num_of_res}.gro"

## USING EDIT CONF TO GENERATE GRO FILE
gmx editconf -f ${output_solvated_pdb} -o ${gro_with_res_num} -box "${new_length}" "${new_length}" "${new_length}"

#################################
### COPYING FORCE FIELD FILES ###
#################################

## COPYING FORCEFIELD FILES 
echo "COPYING FORCE FIELD FILES, ITP, TOPOLOGY, PRM, ETC."

# ITP files
cp -r "${path_input_molecule}/"{${input_molecule_itp},${input_molecule_prm}} "${setup_path}" 

# TOP FILE
cp -r "${top_file_path}/${input_top_file}" "${setup_path}/${topology_file}" 

# MDP FILES
cp -r "${mdp_file_path}/"{${mdp_em},${mdp_equil}} "${setup_path}"

# SUBMIT FILES
cp -r "${submit_file_path}/${input_submit_file}" "${setup_path}/${output_submit_file}"

## EDITING MDP FILES
sed -i "s/_NSTEPS_/${mdp_equil_nsteps}/g" "${mdp_equil}"
sed -i "s/_TEMPERATURE_/${mdp_temp}/g" "${mdp_equil}"

## FINDING RESIDUE NAME FROM ITP FILE
resname=$(itp_get_resname ${input_molecule_itp})

## EDITING TOP FILE
sed -i "s/_MOLECULE_/${solvent_name}/g" "${topology_file}"
echo "   ${resname}    ${num_of_res}" >> "${topology_file}"

## DEFINING OUTPUT NAME
output_job_name="${solvent_name}_${num_of_res}_equil"
sed -i "s/_JOB_NAME_/${output_job_name}/g" "${output_submit_file}"
sed -i "s/_NUM_CORES_/${num_cores}/g" "${output_submit_file}"
sed -i "s/_INPUT_EQUIL_MDP_FILE_/${mdp_equil}/g" "${output_submit_file}"
sed -i "s/_SOLVENT_NAME_/${solvent_name}/g" "${output_submit_file}"
sed -i "s/_NUM_OF_RES_/${num_of_res}/g" "${output_submit_file}"
sed -i "s/_TOPOLOGY_FILE_/${topology_file}/g" "${output_submit_file}"
## UPDATING SOLVENT PATH
sed -i "s#_PATH_SOLVENT_DIR_#${output_path}#g" "${output_submit_file}"\

###################################
### RUNNING ENERGY MINIMIZATION ###
###################################
## GROMPP
gmx grompp -f ${mdp_em} -c "${gro_with_res_num}" -p "${topology_file}" -o "${solvent_name}_${num_of_res}_em"
## MDRUN
gmx mdrun -v -nt 1 -deffnm "${solvent_name}_${num_of_res}_em"

##########################
### ADDING TO JOB LIST ###
##########################

### ADDING TO job list
if [[ "${want_submit}" == true ]]; then
    echo -e "--- CREATING SUBMISSION FILE: ${output_submit_file} ---"
    echo "${setup_path}/${output_submit_file}" >> ${JOB_SCRIPT}
else
    ## RUNNING EQUILIBRATION
    gmx grompp -f "${mdp_equil}" -c "${solvent_name}_${num_of_res}_em.gro" -p "${topology_file}" -o "${solvent_name}_${num_of_res}_equil"
    gmx mdrun -v -nt 12 -deffnm "${solvent_name}_${num_of_res}_equil"

    ## LASTLY, COPYING EQUILIBRATED FILES TO MAIN DIRECTORY
    # GRO FILE
    cp -r "${solvent_name}_${num_of_res}_equil.gro" "${output_path}/${solvent_name}.gro"
    # ITP FILE
    cp -r "${solvent_name}.itp" "${output_path}/${solvent_name}.itp"
    # PRM FILE
    cp -r "${solvent_name}.prm" "${output_path}/${solvent_name}.prm"

fi

### PRINTING SUMMARY
echo "----- SUMMARY -----"
echo "Worked on solvent: ${solvent_name} (${resname})"
echo "Total number of steps for equilibration: ${mdp_equil_nsteps}"
echo "Total solvent residues desired: ${num_of_res}"
echo "*gro, *prm, and *itp located in ${output_path}"
echo "YOU'RE ALL GOOD TO GO! Best of luck!"



